
<!-- Page course -->
<div class="container" style="padding-top: 20px;padding-bottom: 20px">

    <div class="Light card text-black">
        <div class="card-header" style="font-size: 20px">
            หลักสูตรอบรม
        </div>
        <!-- Page Features -->
        <div class="row text-center" style="margin-left: 5px;margin-right: 5px;margin-top: 20px">
    <?php

    $qury = $this->Course->getAllCourse();
    foreach ($qury->result() as $row)
    {
    $image_properties = array(
        'src'   => 'uploads/'.$row->image,
        'class' => 'post_images card-img',
        'width' => '300',
        'height'=> '325',
    );

    ?>

        <div class="col-lg-4 col-sm-6 portfolio-item">
            <div class="card h-100">
                <?php echo  img($image_properties); ?>
                <div class="card-body">
                    <h4 class="card-title"><?php echo $row->course_name ?></h4>
                    <p class="card-text"><?php echo $row->course_details ?></p>
                </div>
                <div class="card-footer">
                    <?php
                    echo "<a href= '".base_url()."course/$row->course_id/detail' class='btn btn-default' style='background-color: #66afe9' role='button'><font size=\"2\" color=\"#f0f8ff\"> เพิ่มเติม</font></a>";
                    ?>
                </div>
            </div>
        </div>

        <?php
        }
        ?>

    </div>
</div>

</div>