<div class="container" style="margin-top: 2%;margin-bottom: 2%">

<?php
foreach ($course_data as $row){
    echo form_hidden('course_id',$course_data[0]->course_id);
    ?>
    <div class="card">
    <div class="card-header text-center"><h3>รายละเอียดการอบรม</h3></div>
    <div class="card-body-icon">
        <!-- List group -->
        <ul class="list-group">

            <li class="list-group-item"><h3>ชื่อหลักสูตร : <?php echo $course_data[0]->course_name?></h3></li>
            <li class="list-group-item"><h4>รายละเอียดหลักสูตร : <?php echo $course_data[0]->course_details?></h4></li>
            <li class="list-group-item"><h4>เนื้อหาหลักสูตร : <?php echo $course_data[0]->course_content?></h4></li>
            <li class="list-group-item"><h4>วัน-เวลาเริ่มอบรม : <?php echo $course_data[0]->date_time_s?></h4></li>
            <li class="list-group-item"><h4>วันสิ้นสุดอบรม : <?php echo $course_data[0]->date_time_e?></h4></li>
            <li class="list-group-item"><h4>จำนวนชั่วโมงที่อบรม : <?php echo $course_data[0]->training_hours?></h4></li>
            <li class="list-group-item"><h4>คุณสมบัตรผู้เข้ารับการอบรม : <?php echo $course_data[0]->training_property?></h4></li>
            <li class="list-group-item"><h4>ระดับหลักสูตร : <?php echo $course_data[0]->course_level?></h4></li>
            <li class="list-group-item"><h4>เหมาะสำหรับ : <?php echo $course_data[0]->appropriability?></h4></li>
            <li class="list-group-item"><h4>โปรแกรมที่ใช้ในการจัดอบรม : <?php echo $course_data[0]->training_program?></h4></li>
            <li class="list-group-item"><h4>ค่าลงทะเบียนหลักสูตร : <?php echo $course_data[0]->cost?></h4></li>
            <li class="list-group-item"><h4>รุ่นที่เปิด : <?php echo $course_data[0]->number_versions?></h4></li>
            <li class="list-group-item"><h4>วิทยากรผู้อบรม : <?php echo $course_data[0]->lecturer?></h4></li>
            <li class="list-group-item"><h4>สถานที่จัดอบรม : <?php echo $course_data[0]->location?></h4></li>
            <li class="list-group-item"><h4>ระยะเวลาเริ่มลงทะเบียน : <?php echo $course_data[0]->registration_period_s?></h4></li>
            <li class="list-group-item"><h4>ระยะเวลาสิ้นสุดลงทะเบียน : <?php echo $course_data[0]->registration_period_e?></h4></li>
            <li class="list-group-item"><h4>จำนวนผู้เข้าอบรม : <?php echo $course_data[0]->amount_of_participants?></h4></li>
        </ul>
        <center>

            <?php


            if ($this->session->userdata('user_type_id') == 1){
                $course_id =  $course_data[0]->course_id;
                echo "<a href= '" . base_url() . "course/$course_id/register' type='button' class='btn btn-info' role='button'>ลงทะเบียนอบรม</a>";

            } elseif ($this->session->userdata('user_type_id') == 2){
                $course_id =  $course_data[0]->course_id;
                echo "<a href= '" . base_url() . "course/$course_id/register' type='button' class='btn btn-info' role='button'>ลงทะเบียนอบรม</a>";

            } elseif ($this->session->userdata('user_type_id') == 3){
                $course_id =  $course_data[0]->course_id;
                echo "<a href= '" . base_url() . "course/$course_id/register' type='button' class='btn btn-info' role='button'>ลงทะเบียนอบรม</a>";

            } elseif ($this->session->userdata('user_type_id') == 4){
                $course_id =  $course_data[0]->course_id;
                echo "<a href= '" . base_url() . "course/$course_id/register' type='button' class='btn btn-info' role='button'>ลงทะเบียนอบรม</a>";

            } else {
                echo "<a href='".site_url() ."user/register' class='btn btn-info' role='button'>สมัครเข้าสู้ระบบ</a>";
            }
            ?></center>
    </div>



    <?php echo form_close(); ?>


<?php }?>
</div>
</div>