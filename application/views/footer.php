
<footer class="py-2 bg-dark">
    <div class="container">
        <div class="row">
        <div class="col-sm-6">
            <p style="font-size: small;color: white" class=""><strong>ที่อยู่</strong><br>
                สำนักวิทยบริการและเทคโนโลยีสารสนเทศ  มหาวิทยาลัยราชภัฏนครสวรรค์<br>
                398 หมู่ 9 ถนนสวรรค์วิถี ตำบลนครสวรรค์ตก อำเภอเมือง จังหวัดนครสวรรค์ 60000<br>
                <strong>ติดต่อ</strong><br>
                website : http://aritc.nsru.ac.th <br>
                เบอร์โทร. 0 5621 9100 -29 ต่อ 1502<br>
                FAX.0 5688 2244<br>
                <center>
                <a href="https://www.facebook.com/pages/ittraining-nsru/193456810706522" target="_blank">
                    <img src="http://localhost/p13_nsru_training/src/img/icon/icon-facebook.png" class="img-responsive" style=""></a></center>
        </div>
        <div class="col-sm-6 map-responsive">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d960.3030158666796!2d100.1057681!3d15.6868921!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e04e4b21437db1%3A0x941b5c9656dbffae!2z4Lit4Liy4LiE4Liy4Lij4LmA4LiJ4Lil4Li04Lih4Lie4Lij4Liw4LmA4LiB4Li14Lii4Lij4LiV4Li0IDgwIOC4nuC4o-C4o-C4qeC4siA1IOC4mOC4seC4meC4p-C4suC4hOC4oSAyNTUw!5e0!3m2!1sth!2sth!4v1519197642450" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        </div>

    </div>
    <!-- /.container -->
</footer>


<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url('vendor/jquery/jquery.min.js')?>"></script>
<script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>


</body>

</html>
