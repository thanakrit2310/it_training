<br>
<div class="container">
    <!---ส่วนใช้งาน--->
    <center>

    <div class="jumbotron">
        <h2>ติดต่อเจ้าหน้าที่</h2>
        <br>
        <br>

        <?php $attributes = array("class" => "form-horizontal", "name" => "contactform");
        echo form_open("Page/contactus", $attributes); ?>
        <input name="skip_WhereToSend" type="hidden" value="your@email.com" />
        <input name="skip_SnapHostID" type="hidden" value="9E99XCUBAFK7" />
        <input name="skip_WhereToReturn" type="hidden" value="http://www.YourWebsiteAddress.com/ThankYouPage.htm" />
        <input name="skip_Subject" type="hidden" value="Contact Us Form" />
        <input name="skip_ShowUsersIp" type="hidden" value="1" />
        <input name="skip_SendCopyToUser" type="hidden" value="1" />
        <script type="text/javascript">
            function ValidateForm(frm) {
                if (frm.Name.value == "") {alert('Name is required.');frm.Name.focus();return false;}
                if (frm.FromEmailAddress.value == "") {alert('Email address is required.');frm.FromEmailAddress.focus();return false;}
                if (frm.FromEmailAddress.value.indexOf("@") < 1 || frm.FromEmailAddress.value.indexOf(".") < 1) {alert('Please enter a valid email address.');frm.FromEmailAddress.focus();return false;}
                if (frm.Comments.value == "") {alert('Please enter comments or questions.');frm.Comments.focus();return false;}
                if (frm.skip_CaptchaCode.value == "") {alert('Enter web form code.');frm.skip_CaptchaCode.focus();return false;}
                return true; }
            function ReloadCaptchaImage(captchaImageId) {
                var obj = document.getElementById(captchaImageId);
                var src = '' + obj.src;
                obj.src = '';
                var date = new Date();
                var pos = src.indexOf('&rad=');
                if (pos >= 0) { src = src.substr(0, pos); }
                obj.src = src + '&rad=' + date.getTime();
                return false; }
        </script>
        <table border="0" cellpadding="5" cellspacing="0" width="600" align="center">
            <tr>
                <td><b>หัวข้อเรื่อง*:</b></td>
                <td><input type="text" placeholder="" name="topic" value="<?php echo set_value('topic'); ?>"class="form-control" style="width:350px;"/></td>
            </tr><tr>
            <tr>
                <td><b>ชื่อ*:</b></td>
                <td><input type="text" placeholder="" name="name" value="<?php echo set_value('name'); ?>" class="form-control" style="width:350px;"/></td>
            </tr><tr>
                <td><b>เบอร์โทรศัพท์:</b></td>
                <td><input type="text" placeholder="" name="phoneNumber" value="<?php echo set_value('phoneNumber'); ?>" class="form-control" style="width:350px;"/></td>
            </tr><tr>
                <td><b>Email address*:</b></td>
                <td><input type="text" placeholder="" name="emailAddress" value="<?php echo set_value('emailAddress'); ?>" class="form-control" style="width:350px;"/></td>
            </tr><tr>
            <tr></tr>
            <td><b>Comments เนื้อหา*:</b></td>
            <td><textarea name="comments" rows="7" class="form-control" cols="40" style="width:350px;"><?php echo set_value('comments'); ?></textarea></td>
            </tr><tr>
                <td colspan="2" align="center"> <br />
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr><td colspan="2" style="padding-bottom:18px;">
                                <!-- Please check our ProCaptcha service which is ad-free:
                                http://www.SnapHost.com/captcha/ProCaptchaOverview.aspx -->
                                <a href="http://www.snaphost.com/captcha/ReadyForms/" alt="email forms html" title="email forms html">

                                </a></td></tr>
                        <tr valign="top"><td> <i>Enter web form code*:</i>
                                <input name="skip_CaptchaCode" class="control" type="text" style="width:80px;" maxlength="6" />
                            </td><td>
                                <a href="http://www.snaphost.com/captcha/ReadyForms/ContactUsForm.aspx"><img id="CaptchaImage" alt="Contact Us form" title="HTML code for Contact Us form"
                                                                                                             style="margin-left:20px;"
                                                                                                             src="http://www.SnapHost.com/captcha/CaptchaImage.aspx?id=9E99XCUBAFK7&ImgType=2" /></a><br />
                                <a href="#" onclick="return ReloadCaptchaImage('CaptchaImage');"><span style="font-size:12px;">reload image</span></a> </td></tr>
                    </table> <br />
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <button name="skip_Submit"  type="submit" class="btn btn-success" value="Submit" />&nbsp&nbspส่ง&nbsp&nbsp
                </td></tr>
        </table><br />
        <?php echo validation_errors(); ?>
        </form>
        <?php echo $this->session->flashdata('msg'); ?>

    </div>
    </center>
</div>