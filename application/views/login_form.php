<?php
if ($this->session->userdata('user_type_id')==1) { ?>
    <!-- Split button -->
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <i class="fa fa-user-o "></i>
            Admin :
            <?php echo $this->session->userdata('name'); ?>
            <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li>
                <center><a href="<?php echo base_url('homeadmin')?>">กลับไปหน้า Admin</a></center>
            </li>
            <li role="separator" class="divider"></li>
            <li>
                <center><a href="<?php echo site_url("logout") ?>">ออกจากระบบ</a></center>
            </li>
        </ul>

    </div>
    <?php
}else if ($this->session->userdata('user_type_id')==2) { ?>
    <!-- Split button -->
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <i class="fa fa-user-o "></i>
            <?php echo $this->session->userdata('name'); ?>
            <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li>
                <center><a href="<?php echo base_url('profile')?>">ข้อมูลส่วนตัว</a></center>
            </li>
            <li>
                <center><a href="<?php echo base_url('user/Training')?>">ประวัติการอบรม</a></center>
            </li>
            <li>
                <center><a href="<?php echo base_url('assessment')?>">ประเมินการอบรม</a></center>
            </li>
            <li role="separator" class="divider"></li>
            <li>
                <center><a href="<?php echo site_url("logout") ?>">ออกจากระบบ</a></center>
            </li>
        </ul>

    </div>


    <?php
}else if ($this->session->userdata('user_type_id')==3) {?>
    <!-- Split button -->
    <div class="dropdown">
        <button  class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <i class="fa fa-user-o "></i>
            <?php echo $this->session->userdata('name'); ?>
            <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li>
                <center><a href="<?php echo base_url('profile')?>"">ข้อมูลส่วนตัว</a></center>
            </li>
            <li>
                <center><a href="<?php echo base_url('user/Training')?>">ประวัติการอบรม</a></center>
            </li>
            <li>
                <center><a href="<?php echo base_url('assessment')?>">ประเมินการอบรม</a></center>
            </li>
            <li role="separator" class="divider">
            </li>
            <li><center><a href="<?php echo site_url("logout") ?>">ออกจากระบบ</a></center></li>
        </ul>

    </div>


    <?php
}else if ($this->session->userdata('user_type_id')==4) {?>
    <!-- Split button -->
    <div class="dropdown">
        <button  class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <i class="fa fa-user-o "></i>
            <?php echo $this->session->userdata('name'); ?>
            <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li>
                <center><a href="<?php echo base_url('profile')?>"">ข้อมูลส่วนตัว</a></center>
            </li>
            <li>
                <center><a href="<?php echo base_url('user/Training')?>">ประวัติการอบรม</a></center>
            </li>
            <li>
                <center><a href="<?php echo base_url('assessment')?>">ประเมินการอบรม</a></center>
            </li>
            <li role="separator" class="divider">
            </li>
            <li><center><a href="<?php echo site_url("logout") ?>">ออกจากระบบ</a></center></li>
        </ul>

    </div>




    <?php
}else {
    ?>

    <button type="button" class="btn btn-primary" onclick="document.getElementById('modal-wrapper').style.display='block'">Login</button>

    <div id="modal-wrapper" class="modal">

        <form class="modal-content animate" method="post" action="<?php echo base_url('login')?>">

            <div class="imgcontainer">
                <span onclick="document.getElementById('modal-wrapper').style.display='none'" class="close" title="Close PopUp">&times;</span>
                <img src="<?php echo base_url('image/icon/IT.png')?>"  alt="Responsive image" class="">
                <!----<h1 style="text-align:center">Login</h1>!---->
            </div>

            <div class="container">
                <div class="form-group">
                    <label for="exampleInputUsername" style="font-size:20px ">ชื่อผู้ใช้งาน</label>
                    <input style="width: 90%;
        padding: 14px 18px;
        margin: 8px 26px;
        display: inline-block;
        border: 1px solid #ccc;
        box-sizing: border-box;
        font-size:16px;" type="text" class="form-control" id="exampleInputUsername" placeholder="Username" name="username" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" style="font-size:20px">รหัสผ่าน</label>
                    <input style="width: 90%;
        padding: 14px 18px;
        margin: 8px 26px;
        display: inline-block;
        border: 1px solid #ccc;
        box-sizing: border-box;
        font-size:16px;" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required>
                </div>
                <center>
                    <?php echo $this->session->flashdata('msg_error'); ?>
                </center>
                <center>
                    <button type="submit" class="btn btn-default" name="submit" value="submit">Login</button>
                    <button type="reset" class="btn btn-default" name="Cancel">Cancel</button>
                </center>
                <input type="checkbox" style="margin-left:26px;margin-top:30px;">&nbsp;เข้าสู้ระบบตลอด

                <center><a style="font-size: 18px;color: blue;" class="btn btn-outline-info" href="<?php echo base_url('register')?>" role="button">สมัครสมาชิก</a></center>
                <center><a style="font-size: 18px" class="btn btn-default" href="#" role="button">ลืมรหัสผ่าน</a></center>
            </div>

        </form>

    </div>
<?php }?>

<script>
    // If user clicks anywhere outside of the modal, Modal will close

    var modal = document.getElementById('modal-wrapper');
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>