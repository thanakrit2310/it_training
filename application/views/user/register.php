<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

<div class="container">
    <br> <p><?php echo $this->session->flashdata('verify_msg'); ?> </p>
    <hr>

    <div class="card bg-light">
        <article class="card-body mx-auto" style="max-width: 400px;">
            <h4 class="card-title mt-3 text-center">ลงทะเบียนสมาชิก</h4>
            <p class="text-center"></p>
            <p class="divider-text">

            </p>

            <form method="post" class="form" id="myform1" name="form1" action="<?php echo base_url('user/register')?>">
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <select name="title_name" class="custom-select" style="max-width: 80px;">
                        <option selected="">----</option>
                        <option value="นาง">นาง</option>
                        <option value="นางสาว">นางสาว</option>
                        <option value="นาย">นาย</option>
                    </select>

                    <input name="name" id="input_name" class="form-control" placeholder="ชื่อ" type="text">
                </div>

                <span class="text-danger"><?php echo form_error('name');?></span>

                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="surname" id="txtsurname" class="form-control" placeholder="นามสกุล" type="text">
                </div> <!-- form-group// -->
                <span class="text-danger"><?php echo form_error('surname');?></span>

                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-calendar"></i> </span>
                    </div>
                    <input class="form-control" id="birthday" name="birthday" placeholder="วันเกิด" type="date"/>
                </div>
                <span class="text-danger"><?php echo form_error('birthday');?></span>

                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input class="form-control" placeholder="เลขประจำตัวประชาชน" id="citizen_id" name="citizen_id" maxlength="13" minlength="13">
                    <span id="citizen_id_result"></span>
                </div>
                <span class="text-danger"><?php echo form_error('citizen_id');?></span>

                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-list"></i> </span>
                    </div>
                    <input type="text" placeholder="สังกัด / หน่วยงาน" name="department" class="form-control">
                </div>
                <span class="text-danger"><?php echo form_error('department');?></span>

                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-list"></i> </span>
                    </div>
                    <select class="form-control" id="user_type_id" name="user_type_id">
                        <option value="">-- ประเภทผู้ใช้ --</option>
                        <option value="2">บุคลากรภายนอก</option>
                        <option value="3">เจ้าหน้าที่@นักศึกษา</option>
                    </select>
                </div>
                <span class="text-danger"><?php echo form_error('user_type_id');?></span>

                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                    </div>
                    <input type="text" placeholder="โทรศัพท์มือถือ" name="telephone_number" class="form-control" maxlength="10" minlength="10">
                </div>
                <span class="text-danger"><?php echo form_error('telephone_number');?></span>

                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                    </div>
                    <input type="text" placeholder="โทรศัพท์หน่วยงาน" name="telephone_number_department" class="form-control" maxlength="11">
                </div>
                <span class="text-danger"><?php echo form_error('telephone_number_department');?></span>

                <!-- form-group// -->
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                    </div>
                    <input name="email" class="form-control" placeholder="email" type="email">
                </div>
                <span class="text-danger"><?php echo form_error('email');?></span>

                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="username" class="form-control" placeholder="Username" type="text">
                </div> <!-- form-group// -->
                <span class="text-danger"><?php echo form_error('username');?></span>

                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                    </div>
                    <input class="form-control" name="password" id="password" placeholder=" Password" type="password">
                </div> <!-- form-group// -->
                <span class="text-danger"><?php echo form_error('password');?></span>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                    </div>
                    <input class="form-control" name="password2" id="passconf" placeholder=" Password อีกครั้ง" type="password">
                </div> <span class="text-danger"><?php echo form_error('password2');?></span><!-- form-group// -->


                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"> ลงทะเบียนสมาชิก </button>
                </div> <!-- form-group// -->

            </form>
        </article>
    </div> <!-- card.// -->

</div>

<!--container end.//-->
<br>
