<?php
$user_id = $this->session->userdata('user_id');

$query = $this->db->query("SELECT * FROM user WHERE user_id = $user_id");

foreach ($query->result_array() as $row)
{
?>
<div class="container" style="background-color: #80e4e9;margin-top: 20px;margin-bottom: 20px">
    <div class="row my-2">
        <div class="col-lg-8 order-lg-2">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">ข้อมูลส่วนตัว</a>
                </li>
                <li class="nav-item">
                    <a href="" data-target="#edit" data-toggle="tab" class="nav-link">แก้ไขข้อมูลส่วนตัว</a>
                </li>
                <li class="nav-item">
                    <a href="" data-target="#change_password" data-toggle="tab" class="nav-link">เปลื่อนรหัสผ่าน</a>
                </li>
            </ul>
            <div class="tab-content py-4">
                <div class="tab-pane active" id="profile">
                    <h4 class="mb-3">ประวัติส่วนตัว</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <h5><b>ข้อมูลทั่วไป</b></h5>
                            <br>
                            <p>
                                <strong>ชื่อ-นามสกุล</strong>     :&nbsp;<?php echo $row['title_name']; echo $row['name'];echo "&nbsp;"; echo $row['surname'];?>
                                <br>
                                <br>
                                <strong>วันเกิด</strong>        :&nbsp;<?php echo $row['birthday'];?>
                                <br>
                                <br>
                                <strong>เลขประจำตัวประชาชน</strong> :&nbsp;<?php echo  $row['citizen_id'];?>
                                <br>
                                <br>
                                <strong>สังกัด / หน่วยงาน</strong> :&nbsp;<?php echo  $row['department'];?>
                                <br>
                                <br>
                                <strong>เบอร์โทรศัพท์</strong> :&nbsp;<?php echo  $row['telephone_number'];?>
                                <br>
                                <br>
                                <strong>เบอร์โทรศัพท์สังกัด/หน่วยงาน</strong> :&nbsp;<?php echo  $row['telephone_number_department'];?>
                                <br>
                                <br>
                                <strong>E-mail</strong> :&nbsp;<?php echo  $row['email'];?>
                            </p>

                        </div>
                        <div class="col-md-6">
                            <h6></h6>
                            <a href="#" class="badge badge-dark badge-pill"></a>
                            <a href="#" class="badge badge-dark badge-pill"></a>
                            <a href="#" class="badge badge-dark badge-pill"></a>
                            <a href="#" class="badge badge-dark badge-pill"></a>
                            <a href="#" class="badge badge-dark badge-pill"></a>
                            <a href="#" class="badge badge-dark badge-pill"></a>
                            <a href="#" class="badge badge-dark badge-pill"></a>
                            <a href="#" class="badge badge-dark badge-pill"></a>
                            <hr>
                            <span class="badge badge-primary"><i class="fa fa-user"></i></span>
                            <span class="badge badge-success"><i class="fa fa-cog"></i></span>
                            <span class="badge badge-danger"><i class="fa fa-eye"></i></span>
                        </div>
                        <div class="col-md-12">


                        </div>
                    </div>
                    <!--/row-->
                </div>
                <div class="tab-pane" id="messages">
                    <div class="alert alert-info alert-dismissable">
                        <a class="panel-close close" data-dismiss="alert">×</a> This is an <strong>.alert</strong>. Use this to show important messages to the user.
                    </div>
                    <table class="table table-hover table-striped">
                        <tbody>
                        <tr>
                            <td>
                                <span class="float-right font-weight-bold">3 hrs ago</span> Here is your a link to the latest summary report from the..
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="float-right font-weight-bold">Yesterday</span> There has been a request on your account since that was..
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="float-right font-weight-bold">9/10</span> Porttitor vitae ultrices quis, dapibus id dolor. Morbi venenatis lacinia rhoncus.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="float-right font-weight-bold">9/4</span> Vestibulum tincidunt ullamcorper eros eget luctus.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="float-right font-weight-bold">9/4</span> Maxamillion ais the fix for tibulum tincidunt ullamcorper eros.
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="edit">
                    <form role="form" action="<?php echo base_url('profile_edit')?>" method="post">

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">ชื่อ-นามสกุล</label>
                            <div class="col-lg-2">
                                <input class="form-control" type="text" name="title_name" value="<?php echo $row['title_name'];
                                ?>" placeholder="คำนำหน้าชื่อ">
                            </div>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" name="name" value="<?php echo $row['name'];
                                ?>" placeholder="">
                            </div>
                            <div class="col-lg-4">
                                <input class="form-control" type="text" name="surname" value="<?php echo $row['surname'];
                                ?>" placeholder="">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">สังกัด/หน่วยงาน</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" name="department"  value="<?php echo $row['department'];
                                ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">โทรศัพท์มือถือ</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" name="telephone_number"  value="<?php echo $row['telephone_number'];
                                ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">โทรศัพท์หน่วยงาน</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" name="telephone_number_department"  value="<?php echo $row['telephone_number_department'];?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">E-mail</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="email" name="email" value="<?php echo $row['email'];
                                ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label"></label>
                            <div class="col-lg-9 text-center">
                                <input type="submit"  class="btn btn-primary" value="Save Changes">
                                <input type="reset" class="btn btn-secondary" value="Cancel">

                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="change_password">
                    <form role="form" action="<?php echo base_url('change_password')?>" method="post">

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">รหัสผ่านเดิม</label>
                            <div class="col-lg-6">
                                <input class="form-control" type="password" name="password" id="myInput" value="">
                                <input type="checkbox" onclick="myFunction()">Show Password
                            </div>
                            <span class="text-danger"><?php echo form_error('password');?></span>
                            <div class="col-lg-3"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">รหัสผ่านใหม่</label>
                            <div class="col-lg-6">
                                <input class="form-control" type="password" name="password_new" id="myInput2" value="">
                                <input type="checkbox" onclick="myFunction()">Show Password
                            </div>
                            <span class="text-danger"><?php echo form_error('password_new');?></span>
                            <div class="col-lg-3"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">ยืนยันรหัสผ่านใหม่</label>
                            <div class="col-lg-6">
                                <input class="form-control" type="password" name="password_new2" id="myInput3" value="">
                                <input type="checkbox" onclick="myFunction()">Show Password
                            </div>
                            <span class="text-danger"><?php echo form_error('password_new2');?></span>
                            <div class="col-lg-3"></div>
                        </div>


                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label"></label>
                            <div class="col-lg-6 text-center">
                                <input type="submit"  class="btn btn-primary" value="Save Changes">
                                <input type="reset" class="btn btn-secondary" value="Cancel">

                            </div>
                            <div class="col-lg-3"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-4 order-lg-1 text-center">
            <br>
            <img src="<?php echo base_url('image/profile/pro.png')?>" class="mx-auto img-fluid img-circle d-block" alt="avatar">
            <h4 class="mt-2">ยินดีต้อนรับคุณ : <?php echo $this->session->userdata('name'); ?></h4>

            <!---<label class="custom-file">
                <input type="file" id="file" class="custom-file-input">
                <span class="custom-file-control">Choose file</span>
            </label>!--->
        </div>
    </div>
</div>
<?php }?>
<script>
    // sandbox disable popups
    if (window.self !== window.top && window.name!="view1") {;
        window.alert = function(){/*disable alert*/};
        window.confirm = function(){/*disable confirm*/};
        window.prompt = function(){/*disable prompt*/};
        window.open = function(){/*disable open*/};
    }

    // prevent href=# click jump
    document.addEventListener("DOMContentLoaded", function() {
        var links = document.getElementsByTagName("A");
        for(var i=0; i < links.length; i++) {
            if(links[i].href.indexOf('#')!=-1) {
                links[i].addEventListener("click", function(e) {
                    console.debug("prevent href=# click");
                    if (this.hash) {
                        if (this.hash=="#") {
                            e.preventDefault();
                            return false;
                        }
                        else {
                            /*
                            var el = document.getElementById(this.hash.replace(/#/, ""));
                            if (el) {
                              el.scrollIntoView(true);
                            }
                            */
                        }
                    }
                    return false;
                })
            }
        }
    }, false);


    function myFunction() {
        var x = document.getElementById("myInput");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }function myFunction2() {
        var x = document.getElementById("myInput2");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }function myFunction3() {
        var x = document.getElementById("myInput3");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>