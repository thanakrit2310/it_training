<br>
<div class="container">
    <?php

    $user_id=$this->session->userdata('user_id');

    $sql    = "SELECT * FROM course_register
LEFT JOIN user ON (user.user_id=course_register.user_id)
LEFT JOIN course ON (course_register.course_id=course.course_id)
where user.user_id = $user_id;";
    $result = $this->db->query($sql)->result();
    ?>

    <div class="clearfix visible-xs-block"></div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <!--<div class="panel-heading"><h4>รายละเอียดการอบรม</h4></div>--->
        <!--Table-->
        <!--<table   class="table table-condensed" style="font-size:small">-->
        <div class="card mb-3">
            <div class="card-header">
                <h3>ประเมินการอบรม</h3></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="font-size: small">
                        <thead >
                        <tr>

                            <th>ชื่อหลักสูตร</th>
                            <th>สถานที่</th>
                            <th>วัน-เวลาเริ่มอบรม</th>
                            <th>วัน-เวลาสิ้นสุดอบรม</th>
                            <th>ค่าใช้จ่าย</th>
                            <th>วิทยากร</th>
                            <th>ประเมินหลักสูตรที่อบรม</th>

                        </tr>
                        </thead>
                        <?php
                        foreach ($result as $row) {
                            //echo "<td>".$row->course_id."</td>";

                            echo "<td>" . $row->course_name . "</td>";
                            echo "<td>" . $row->location . "</td>";
                            echo "<td>" . $row->date_time_s . "</td>";
                            echo "<td>" . $row->date_time_e . "</td>";
                            echo "<td>" . $row->cost . "</td>";
                            echo "<td>" . $row->lecturer . "</td>";


                            if ($row->status_assessment==0){
                                echo "<td><a href='" . site_url() . "user/$row->course_register_id/$row->course_id/confirm' class='btn btn-default btn-sm'><i class=\"fa fa-fw fa-edit\"></i>ประเมิน</a></td>";
                            }elseif ($row->status_assessment>0){
                                echo "ประเมินเรียบแล้ว";
                                //echo "<td><a href='" . site_url() . "user/$row->course_register_id/$row->course_id/payment' class='btn btn-default btn-sm'><i class=\"fa fa-fw fa-edit\"></i>ประเมิน</a></td>";
                            }


                            echo "</tr>";
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>