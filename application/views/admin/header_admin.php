
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>IT_Admin</title>
        <!-- Bootstrap core CSS-->
        <link href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url('css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
        <!-- Page level plugin CSS-->
        <link href="<?php echo base_url('vendor/datatables/dataTables.bootstrap4.css')?>" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="<?php echo base_url('css/sb-admin.css')?>" rel="stylesheet">

        <!-----icon----->
        <link rel="icon" href="<?php echo base_url('image/icon/icon_it.ico')?>" type="image/x-icon">
        <link rel="shortcut icon" href="<?php echo base_url('image/icon/icon_it.ico')?>" type="image/x-icon">
    </head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
        <a class="navbar-brand" href="<?php echo base_url('home')?>">Admin : <?php echo $this->session->userdata('name'); ?></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="หน้าแรก">
                    <a class="nav-link" href="<?php echo base_url('home')?>">
                        <i class="fa fa-fw fa-home"></i>
                        <span class="nav-link-text">หน้าแรก</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="จัดการข้อมูลหลักสูตร">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
                        <i class="fa fa-fw fa-wrench"></i>
                        <span class="nav-link-text">จัดการข้อมูลหลักสูตร</span>
                    </a>
                    <ul class="sidenav-second-level collapse" id="collapseComponents">
                        <li>
                            <a href="<?php echo site_url('admin/course/add')?>">เพิ่มหลักสูตรอบรม</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('admin/course/select_edit')?>">แก้ไขหลักสูตรอบรม</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('')?>">สถานะค่าใช้จ่ายหลักสูตร</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="จัดการข้อมูลผู้อบรม">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
                        <i class="fa fa-fw fa-wrench"></i>
                        <span class="nav-link-text">จัดการข้อมูลผู้อบรม</span>
                    </a>
                    <ul class="sidenav-second-level collapse" id="collapseExamplePages">
                        <li>
                            <a href="<?php echo site_url('admin/user/manage')?>">แก้ไขข้อมูลผู้เข้าอบรม</a>
                        </li>
                        <li>
                            <a href="">พิมพ์ใบรายชื่อผู้เข้าอบรม</a>
                        </li>
                        <li>
                            <a href="">ออกใบวุฒิบัตรให้ผู้อบรม</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="จัดการข้อมูลข่าวสาร">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
                        <i class="fa fa-fw fa-bullhorn"></i>
                        <span class="nav-link-text">จัดการข้อมูลข่าวสาร</span>
                    </a>
                    <ul class="sidenav-second-level collapse" id="collapseMulti">
                        <li>
                            <a href="#">Second Level Item</a>
                        </li>
                        <li>
                            <a href="#">Second Level Item</a>
                        </li>
                        <li>
                            <a href="#">Second Level Item</a>
                        </li>
                        <li>
                            <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti2">Third Level</a>
                            <ul class="sidenav-third-level collapse" id="collapseMulti2">
                                <li>
                                    <a href="#">Third Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Third Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Third Level Item</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="จัดการข้อมูลผู้อบรม">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages2" data-parent="#exampleAccordion">
                        <i class="fa fa-fw fa-file-alt"></i>
                        <span class="nav-link-text">ออกรายงาน</span>
                    </a>
                    <ul class="sidenav-second-level collapse" id="collapseExamplePages2">
                        <li>
                            <a href="">รายงานข้อมูลหลักสูตรอบรม</a>
                        </li>
                        <li>
                            <a href="">รายงานข้อมูลการประเมิน</a>
                        </li>
                    </ul>
                </li>

            </ul>
            <ul class="navbar-nav sidenav-toggler">
                <li class="nav-item">
                    <a class="nav-link text-center" id="sidenavToggler">
                        <i class="fa fa-fw fa-angle-left"></i>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link"   href="<?php echo site_url("home") ?>">
                        <i class="fa fa-fw fa-home"></i>หน้า IT Training</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link"   href="<?php echo base_url('logout') ?>">
                        <i class="fa fa-fw fa-sign-out-alt"></i>Logout</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="content-wrapper">
        <div class="container-fluid">

            <!-- Breadcrumbs-->
            <!---<ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">My Dashboard</li>
            </ol>---->
