</div>
<!-- /.container-fluid-->
<!-- /.content-wrapper-->
<footer class="sticky-footer">
    <div class="container">
        <div class="text-center">
            <small>2017 Nakhon Sawan Rajabhat University © Created by Thanakrit Ritthong</small>
        </div>
    </div>
</footer>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>


<!-- Bootstrap core JavaScript-->
<script src="<?php echo base_url('vendor/jquery/jquery.min.js')?>"></script>
<script src="<?php echo base_url('vendor/popper/popper.min.js')?>"></script>
<script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.min.js')?>"></script>
<!-- Core plugin JavaScript-->
<script src="<?php echo base_url('vendor/jquery-easing/jquery.easing.min.js')?>"></script>
<!-- Page level plugin JavaScript-->

<script src="<?php echo base_url('vendor/datatables/jquery.dataTables.js')?>"></script>
<script src="<?php echo base_url('vendor/datatables/dataTables.bootstrap4.js')?>"></script>
<!-- Custom scripts for all pages-->
<script src="<?php echo base_url('js/sb-admin.min.js')?>"></script>
<!-- Custom scripts for this page-->

<script defer src="https://use.fontawesome.com/releases/v5.2.0/js/all.js" integrity="sha384-4oV5EgaV02iISL2ban6c/RmotsABqE4yZxZLcYMAdG7FAPsyHYAPpywE9PJo+Khy" crossorigin="anonymous"></script>
</div>
</body>

</html>