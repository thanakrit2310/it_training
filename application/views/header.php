<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>IT-TRAINING</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('css/3-col-portfolio.css')?>" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.0.0/css/bootstrap-datetimepicker.css" rel="stylesheet">

    <!-----icon----->
    <link rel="icon" href="<?php echo base_url('image/icon/icon_it.ico')?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo base_url('image/icon/icon_it.ico')?>" type="image/x-icon">

    <link href="<?php echo base_url('css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

</head>
<style type="text/css">
    body {
        background: url(<?php echo base_url("image/background/BG1.jpg")?>) no-repeat center center fixed;
        background-size: cover;
    }
    *{margin:0px; padding:0px; font-family:Helvetica, Arial, sans-serif;}

    /* Full-width input fields */

    /* Set a style for all buttons */

    button:hover {
        opacity: 0.8;
    }

    /* Center the image and position the close button */
    .imgcontainer {
        text-align: center;
        margin: 24px 0 12px 0;
        position: relative;
    }
    .avatar {
        width: 200px;
        height:200px;
        border-radius: 50%;
    }

    /* The Modal (background) */
    .modal {
        display:none;
        position: fixed;
        z-index: 1;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        overflow-y: auto;
        background-color: rgba(0,0,0,0.4);
    }

    /* Modal Content Box */
    .modal-content {
        background-color: #fefefe;
        margin: 4% auto 15% auto;
        border: 1px solid #888;
        padding-bottom: 30px;

        position: relative;
        width: 90%;
        max-width: 600px;
    }

    /* The Close Button (x) */
    .close {
        position: absolute;
        right: 25px;
        top: 0;
        color: #000;
        font-size: 35px;
        font-weight: bold;
    }
    .close:hover,.close:focus {
        color: red;
        cursor: pointer;
    }

    .card {
        height: 100%;
    }

    .map-responsive{
        overflow:hidden;
        padding-bottom:15%;
        position:relative;
        height:0;
    }
    .map-responsive iframe{
        left:0;
        top:0;
        height:100%;
        width:100%;
        position:absolute;
    }


    /* Add Zoom Animation */
    .animate {
        animation: zoom 0.6s
    }
    @keyframes zoom {
        from {transform: scale(0)}
        to {transform: scale(1)}
    }

</style>
<body>

<!-- Navigation -->
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="<?php echo base_url('home')?>">งานฝึกอบรมคอมพิวเตอร์</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo base_url('home')?>">หน้าแรก
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('history')?>">งานฝึกอบรมตอมพิวเตอร์</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('ittraining')?>">ข้อมูลการฝึกอบรม</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('contactus')?>">ติดต่อ</a>
                </li>
            </ul>
            <?php

            $this->load->view('login_form');

            ?>
        </div>
    </div>
</nav>
<!-- Header - set the background image for the header in the line below -->
<header class="py-15 bg-image-full" style="background-image: url('<?php echo base_url('image/background/bg01.png')?>');">
<img class="img-fluid d-block mx-auto" src="<?php echo base_url('image/slide/it_training.png')?>" alt="">
</header>
