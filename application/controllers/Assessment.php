<?php
/**
 * Created by PhpStorm.
 * User: Gja_Jammee
 * Date: 20/8/2561
 * Time: 11:31
 */

class Assessment extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        $this->load->library('user_agent');  /// เรียกใช้ user agent class
        $this->load->helper('html');
    }

    public function assessment ()
    {

        $this->load->view('header');
        $this->load->view('user/assessment');
        $this->load->view('footer');

    }
}