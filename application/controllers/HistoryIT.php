<?php
/**
 * Created by PhpStorm.
 * User: Gja_Jammee
 * Date: 23/4/2561
 * Time: 13:45
 */

class historyIT extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        $this->load->library('user_agent');  /// เรียกใช้ user agent class
        $this->load->helper('html');
    }

    public  function  history()
    {
        $user_id = $this->session->userdata('user_id');
        $header_data = array(
            'user_id' => $user_id,
            'msg_error' => $this->session->flashdata('msg_error')
        );


        $this->load->view('header',$header_data);
        $this->load->view('history_IT/bodyhistory');
        $this->load->view('footer');

    }


}