<?php
/**
 * Created by PhpStorm.
 * User: Gja_Jammee
 * Date: 21/2/2561
 * Time: 11:34
 */

class Page extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        $this->load->library('user_agent');  /// เรียกใช้ user agent class
        $this->load->helper('html');
    }

    public function home()
    {
        $user_id = $this->session->userdata('user_id');
        $this->load->model("Course");
        $header_data = array(
            'user_id' => $user_id,
            'msg_error' => $this->session->flashdata('msg_error')
        );


        $this->load->view('header',$header_data);
        $this->load->view('course/course_show');
        $this->load->view('news');
        //$this->load->view('course/course_view');
        $this->load->view('footer');
    }

    public function contactus()
    {
        //set validation rules
        $this->form_validation->set_rules('topic','Topic','required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('phoneNumber', 'PhoneNumber', 'required');
        $this->form_validation->set_rules('emailAddress', 'EmailAddress', 'required');
        $this->form_validation->set_rules('comments', 'Comments', 'required');

        //run validation on form input
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('header');
            $this->load->view('contactus');
            $this->load->view('footer');
        }
        else
        {
            //get the form data
            $topic = $this->input->post('topic');
            $name = $this->input->post('name');
            $emailAddress = $this->input->post('emailAddress');
            $phoneNumber = $this->input->post('phoneNumber');
            $comments = $this->input->post('comments');

            //set to_email id to which you want to receive mails
            $to_email = 'thanakrit9966@gmail.com';

            //configure email settings
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'ssl://smtp.gmail.com';
            $config['smtp_port'] = '465';
            $config['smtp_user'] = 'thanakrit9966@gmail.com';
            $config['smtp_pass'] = '5a0a38xz23';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;
            $config['newline'] = "\r\n"; //use double quotes
            //$this->load->library('email', $config);
            $this->email->initialize($config);

            //send mail
            $this->email->from($emailAddress, $name);
            $this->email->to($to_email);
            //$this->email->phoneNumber($phoneNumber);
            $this->email->subject($topic);
            $this->email->message($phoneNumber,$comments);
            if ($this->email->send())
            {
                // mail sent
                $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Your mail has been sent successfully!</div>');
                redirect('Page/contactus');
            }
            else
            {
                //error
                $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">There is error in sending mail! Please try again later</div>');
                redirect('Page/contactus');
            }
        }
    }

//custom validation function to accept only alphabets and space input
    public function alpha_space_only($str)
    {
            if (!preg_match("/^[a-zA-Z ]+$/",$str))
            {
            $this->form_validation->set_message('alpha_space_only', 'The %s field must contain only alphabets and space');
            return FALSE;
            }
            else
            {
            return TRUE;
            }
    }

    public function  HomeAdmin()
    {
        $this->load->view('admin/header_admin');
        $this->load->view('admin/body_admin');
        $this->load->view('admin/footer_admin');
    }
}