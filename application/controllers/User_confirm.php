<?php
/**
 * Created by PhpStorm.
 * User: Gja_Jammee
 * Date: 24/7/2561
 * Time: 10:51
 */

class User_confirm extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        $this->load->library('user_agent');  /// เรียกใช้ user agent class
        $this->load->helper('html');
    }

    public function confirm($course_register_id,$course_id)
    {
        $this->load->model('Courseregister');
        $data = array(
            'course_id' => $course_id,
            'user_id' => $this->session->userdata('user_id'),
            'payment_status' =>"ยืนยันเข้าอบรม",
            'confirm_status' =>"ยืนยันเข้าอบรม"
        );
        $this->Courseregister->confirm($data,$course_register_id);

    }

    public function payment($course_register_id,$course_id)
    {
        $data = array(
            'course_id' => $course_id,
            'course_register_id' => $course_register_id
        );
        $this->load->view('header');
        $this->load->view('user/payment',$data);
        $this->load->view('footer');
    }

}