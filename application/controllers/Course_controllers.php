<?php
/**
 * Created by PhpStorm.
 * User: Gja_Jammee
 * Date: 24/7/2561
 * Time: 13:07
 */

class Course_controllers extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('array','url');
        $this->load->model('User');
        $this->load->model('Course');
        $this->load->model('Courseregister');
    }

    public function detail($course_id)
    {


        $data  = [
            'course_data' => $this->Course->get($course_id)
        ];
        $this->load->view('header');
        $this->load->view('course/detail',$data);
        $this->load->view('footer');

    }

    public function course()
    {
        $this->load->view('course/view');
    }
    public function user_list()
    {

    }

    public function document()
    {

    }

    public function register($course_id)
    {
        $this->load->model('Course');
        $this->load->model('User');

        $data = [
            'course_re' => $this->Course->get($course_id),
            'course_gi' => $this->session->userdata('user_id')
        ];
        $this->load->view('course/course_regis',$data);
    }
    public function save_register()
    {

        $new_register = clone $this->Courseregister;
        $new_register->course_id = $this->input->post('course_id');
        $new_register->user_id = $this->session->userdata('user_id');
        $new_register->payment_status = 'รอการยืนยัน';
        $new_register->confirm_status = 'รอการยืนยัน';

        $new_register->insert();

    }

    public function payment()
    {

    }
}
