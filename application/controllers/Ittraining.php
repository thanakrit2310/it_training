<?php
/**
 * Created by PhpStorm.
 * User: Gja_Jammee
 * Date: 3/8/2561
 * Time: 16:06
 */

class Ittraining extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        $this->load->library('user_agent');  /// เรียกใช้ user agent class
        $this->load->helper('html');
    }

    public function ittraining ()
    {
        $user_id = $this->session->userdata('user_id');
        $header_data = array(
            'user_id' => $user_id,
            'msg_error' => $this->session->flashdata('msg_error')
        );


        $this->load->view('header',$header_data);
        $this->load->view('ittraining/ittraining');
        $this->load->view('footer');
    }

}