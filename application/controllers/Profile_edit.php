<?php
/**
 * Created by PhpStorm.
 * User: Gja_Jammee
 * Date: 1/8/2561
 * Time: 14:33
 */

class Profile_edit extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        $this->load->library('user_agent');  /// เรียกใช้ user agent class
        $this->load->helper('html');
    }

    public function profile_edit ()
    {
        $data1 = array(
            'required'      => 'กรุณากรอก %s.ด้วยคะ'
        );

        $this->form_validation->set_rules('title_name','คำนำหน้าชื่อ','required',$data1);
        $this->form_validation->set_rules('name','ชื่อ','required',$data1);
        $this->form_validation->set_rules('surname','นามสกุล','required',$data1);
        $this->form_validation->set_rules('department','สังกัด/หน่วยงาน','required',$data1);
        $this->form_validation->set_rules('telephone_number','หมายเลขโทรศัพท์','required',$data1);
        $this->form_validation->set_rules('telephone_number_department','หมายเลขโทรศัพท์หน่วยงาน','required',$data1);
        //$this->form_validation->set_rules('email','Email','required',$data1);



        if ($this->form_validation->run() == FALSE) {

            redirect('profile');

        } else {

            $this->load->model('User');

            $user_id = $this->session->userdata('user_id');
            $title_name = $this->input->post('title_name');
            $name = $this->input->post('name');
            $surname = $this->input->post('surname');
            $department = $this->input->post('department');
            $telephone_number = $this->input->post('telephone_number');
            $telephone_number_department = $this->input->post('telephone_number_department');
            $email = $this->input->post('email');


            $data = array(
                'title_name' => $title_name,
                'name' => $name,
                'surname' => $surname,
                'department' => $department,
                'telephone_number' => $telephone_number,
                'telephone_number_department' => $telephone_number_department,
                'email' => $email
            );
            $this->User->edit($data, $user_id);
        }

    }

}