<?php
/**
 * Created by PhpStorm.
 * User: Gja_Jammee
 * Date: 3/8/2561
 * Time: 13:37
 */

class Change_password extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        $this->load->library('user_agent');  /// เรียกใช้ user agent class
        $this->load->helper('html');
    }

    public function change_password()
    {


        $data1 = array(
            'required' => 'กรุณากรอก %s.ด้วยคะ'
        );
        $this->form_validation->set_rules('password', 'Password เดิม', 'trim|required|min_length[4]|max_length[32]', $data1);
        $this->form_validation->set_rules('password_new', 'Password ใหม่', 'trim|required|min_length[4]|max_length[32]', $data1);
        $this->form_validation->set_rules('password_new2', 'Password ยืนยัน', 'trim|required|matches[password_new]', $data1);

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('header');
            $this->load->view('user/profile');
            $this->load->view('footer');
        } else {

            $this->load->model('User');

            $user_id = $this->session->userdata('user_id');

            $password_new  = $this->input->post('password_new');

            $data =array(
                'password' => $password_new
            );
            $this->User->change_password($data, $user_id);
        }
    }
}