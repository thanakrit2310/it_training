<?php
/**
 * Created by PhpStorm.
 * User: Gja_Jammee
 * Date: 10/5/2561
 * Time: 12:56
 */

class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        $this->load->library('user_agent');  /// เรียกใช้ user agent class
        $this->load->helper('html');
    }
    public function index()
    {
        $this->load->view('header');
        $this->load->view('user/register');
        $this->load->view('footer');

    }
    public function register()
    {
        $data1 = array(
        'required'      => 'กรุณากรอก %s.ด้วยคะ'
        );


        $this->form_validation->set_rules('title_name','คำนำหน้าชื่อ','required',$data1);
        $this->form_validation->set_rules('name','ชื่อ','required',$data1);
        $this->form_validation->set_rules('surname','นามสกุล','required',$data1);
        $this->form_validation->set_rules('birthday','วันเกิด','required',$data1);
        $this->form_validation->set_rules('citizen_id','เลขประจำตัวประชาชน','trim|required|min_length[13]|max_length[13]',$data1);
        $this->form_validation->set_rules('department','สังกัด/หน่วยงาน','required',$data1);
        $this->form_validation->set_rules('telephone_number','หมายเลขโทรศัพท์','required',$data1);
        $this->form_validation->set_rules('telephone_number_department','หมายเลขโทรศัพท์หน่วยงาน','required',$data1);
        $this->form_validation->set_rules('email','Email','required',$data1);
        $this->form_validation->set_rules('username','Username','required',$data1);
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]',$data1);
        $this->form_validation->set_rules('password2', 'Password ยืนยัน', 'trim|required|matches[password]',$data1);
        $this->form_validation->set_rules('user_type_id','','required',$data1);

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('header');
            $this->load->view('user/register');
            $this->load->view('footer');
        } else {

            $this->load->model('User');

            $title_name = $this->input->post('title_name');
            $name = $this->input->post('name');
            $surname = $this->input->post('surname');
            $birthday = $this->input->post('birthday');
            $citizen_id = $this->input->post('citizen_id');
            $department = $this->input->post('department');
            $telephone_number = $this->input->post('telephone_number');
            $telephone_number_department = $this->input->post('telephone_number_department');
            $email = $this->input->post('email');
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $user_type_id = $this->input->post('user_type_id');

            $data = array(
                'title_name' => $title_name,
                'name' => $name,
                'surname' => $surname,
                'birthday' => $birthday,
                'citizen_id' => $citizen_id,
                'department' => $department,
                'telephone_number' => $telephone_number,
                'telephone_number_department' => $telephone_number_department,
                'email' => $email,
                'username' => $username,
                'password' => $password,
                'user_type_id' => $user_type_id);

            $this->User->insert($data);
        }
    }

       public function verify($hash=NULL)
        {
            if ($this->User->verifyEmailID($hash))
            {
                $this->session->set_flashdata('verify_msg','<div class="alert alert-success text-center">Your Email Address is successfully verified! Please login to access your account!</div>');
                redirect('user/register');
            }
            else
            {
                $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Sorry! There is error verifying your Email Address!</div>');
                redirect('user/register');
        }
        }

}

