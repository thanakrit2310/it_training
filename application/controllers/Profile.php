<?php
/**
 * Created by PhpStorm.
 * User: Gja_Jammee
 * Date: 2/6/2561
 * Time: 21:09
 */

class Profile extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        $this->load->library('user_agent');  /// เรียกใช้ user agent class
        $this->load->helper('html');
    }

    public function profile()
    {

        $this->load->view('header');
        $this->load->view('user/profile');
        $this->load->view('footer');

    }

}