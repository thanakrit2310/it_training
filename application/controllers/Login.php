<?php
/**
 * Created by PhpStorm.
 * User: Gja_Jammee
 * Date: 22/4/2561
 * Time: 16:56
 */

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library("pagination");
    }

    public function dologin()
    {
        if ($this->input->post('submit')) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $this->load->model('User');
            $check = $this->User->_checkUser($username, $password);
            if ($check) {

                $userdata = $this->User->find($check);

                $data = array(

                    'username' => $username,
                    'user_id' =>$userdata->user_id,
                    'title_name' =>$userdata->title_name,
                    'name' => $userdata->name,
                    'surname' =>$userdata->surname,
                    'department' =>$userdata->department,
                    'telephone_number' =>$userdata->telephone_number,
                    'telephone_number_department' =>$userdata->telephone_number_department,
                    'email'=>$userdata->email,
                    'username'=>$userdata->username,
                    'password' =>$userdata->password,
                    'user_type_id' => $userdata->user_type_id,
                    'logged' => TRUE
                );
                $this->session->set_userdata($data);
                if ($this->session->userdata('user_type_id') == 1) {
                    redirect('homeadmin');
                }
                elseif ($this->session->userdata('user_type_id') == 2) {
                    redirect('home');
                }
                elseif ($this->session->userdata('user_type_id') == 3) {
                    redirect('home');
                } else {
                    redirect('home');
                }

            } else {
                $this->session->set_flashdata('msg_error', 'ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง');
                redirect('home');
            }
        } else {
            redirect('home');
        }

    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url() . "index.php/home");


    }
}