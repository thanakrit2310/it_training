<?php
/**
 * Created by PhpStorm.
 * User: Gja_Jammee
 * Date: 24/7/2561
 * Time: 10:18
 */

class Training extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        $this->load->library('user_agent');  /// เรียกใช้ user agent class
        $this->load->helper('html');
    }

    public  function training()
    {
        $this->load->view('header');
        $this->load->view('user/training');
        $this->load->view('footer');
    }
}