<?php
/**
 * Model ของหลักสูตร
 *
 * @author Thanakrit Ritthong <thanakrit9966@gmail.com>
 */
class Course extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper( 'array','url');


    }

    /**
     * เรียกข้อมูลหลักสูตรทั้งหมด
     *
     * @return array ข้อมูลของหลักสูตรทั้งหมด
     */
    public function getAllCourse()
    {
        $this->db->limit(9);
        return $this->db->get('course');
        /*$sql    = "SELECT * FROM course";
        $course = $this->db->query($sql)->result();

        return $course;*/
    }

    /**
     * เรียกข้อมูลหลักสูตร
     *
     * @param int $course_id
     * @return array ข้อมูลของหลักสูตรตาม course_id ที่ส่งเข้าไป
     */
    public function get($course_id)
    {
        $query = $this->db->get_where("course",array("course_id"=>$course_id));

        $data = $query->result();

        return $data;
    }

    public function get_re($course_register_id){
        $query = $this->db->get_where("course_register",array("course_register_id"=>$course_register_id));

        $data = $query->result();

        return $data;
    }

    /**
     * สร้างหลักสูตรใหม่
     *
     * @return mixed ผลลัพธ์ของการสร้างหลักสูตรเป็น "รหัสหลักสูตรที่สร้างใหม่" ถ้าสำเร็จ และเป็น FALSE ถ้าไม่สำเร็จ
     */
    public function create()
    {
        $this->db->where('course_id');
        $this->db->create('CourseClass');

        if (empty($course_id))
        {
            echo TRUE('รหัสหลักสูตรที่สร้างใหม่');
        }else{
            echo FALSE('สร้างไม่สำเร็จ');
        }

        // TODO เจน ทำตรงนี้ตามรายละเอียดใน Documentation ด้านบน
    }

    /**
     * แก้ไขข้อมูลหลักสูตรเป็นราย Field
     *
     * @param string $field ชื่อ Field ที่ต้องการจะแก้ไขข้อมูล
     * @param string $value ข้อมูลใหม่ที่จะแก้ไข
     * @param int $course_id รหัสหลักสูตร
     * @return boolean ผลลัพธ์ของการแก้ไขหลักสูตรเป็น TRUE ถ้าสำเร็จ และเป็น FALSE ถ้าไม่สำเร็จ
     */

    public function add($data)
    {
            $this->db->insert('course', $data);
            echo "<script>alert('เพิ่มหลักสูตรสำเร็จแล้ว');setTimeout(function () {
   window.location.href= '".site_url("admin/home")."';},3000);</script>";


            return $this->db->insert_id();


    }

    public function set($field, $value, $course_id)
    {
        $this->load->helper('url');

        $value = url_title($this->input->post('field'),'value', TRUE);

        $data = array(
            'field' => $this->input->post('field'),
            'value' => $value,
            'course_id' => $this->input->post('course_id')
        );

        return $this->db->insert('news', $data);
        //$this->db->set($field,$value='',$course_id = TRUE);
        // TODO เจน ทำตรงนี้ตามรายละเอียดใน Documentation ด้านบน
    }

    /**
     * เรียกข้อมูลหลักสูตร
     *
     * @param int $course_id
     * @return boolean ผลลัพธ์ของการลบหลักสูตรเป็น TRUE ถ้าสำเร็จ และเป็น FALSE ถ้าไม่สำเร็จ
     */
    public function delete($course_id)
    {
        if ($this->db->delete("course", "course_id = " . $course_id)) {
            return true;

            // TODO เจน ทำตรงนี้ตามรายละเอียดใน Documentation ด้านบน
        }
    }
    public function update($data,$course_id)
    {
        $this->db->update('course', $data, array('course_id' =>$course_id));

    }


}