<?php
/**
 * Created by PhpStorm.
 * User: Sejan
 * Date: 7/27/2017
 * Time: 1:54 PM
 */

class Courseregister extends CI_Model
{
    public $course_register_id ="";
    //public $number_of_registrants ="";
    public $payment_status ="";
    public $confirm_status ="";
    public $user_id ="";
    public $course_id ="";

    public function __construct()
    {
    parent::__construct();
        $this->load->helper('array','url');
        $this->load->helper('html');
    }

    public function insert()
    {
        $data = array(
            'course_register_id' => $this->course_register_id,
            'confirm_status' => $this->confirm_status,
            'payment_status' => $this->payment_status,
            'user_id' => $this->session->userdata('user_id'),
            'course_id' => $this->course_id
        );
        echo "<script>alert('ลงทะเบียนสำเร็จแล้ว');setTimeout(function () {
   window.location.href= '".site_url("home")."';},1000);</script>";

        return $this->db->insert('course_register',$data);

    }

    public function confirm($data,$course_register_id)
    {
        $this->db->update('course_register', $data, array('course_register_id' =>$course_register_id));
        echo "<script>alert('ยีนยันเรียบร้อยแล้ว');setTimeout(function () {
   window.location.href= '".site_url("user/Training")."';},2000);</script>";
    }

    public function payment($data,$course_register_id)
    {
        $this->db->update('course_register', $data, array('course_register_id' =>$course_register_id));
        echo "<script>alert('ยีนยันเรียบร้อยแล้ว');setTimeout(function () {
   window.location.href= '".site_url("user/Training")."';},2000);</script>";
    }

    public function delete()
    {
        $data = array(
            'course_register_id' => $this->course_register_id,
            'number_of_registrants' => $this->number_of_registrants,
            'payment_status' => $this->payment_status,
            'user_id' => $this->user_id,
            'course_id' => $this->course_id
        );
        return $this->db->delete('course_register',$data);
    }

    public function select()
    {
        $data = array(
            'course_register_id' => $this->course_register_id,
            'number_of_registrants' => $this->number_of_registrants,
            'payment_status' => $this->payment_status,
            'user_id' => $this->user_id,
            'course_id' => $this->course_id
        );
        return $this->db->get('course_register',$data);
    }

    public function select_all()
    {
        $data = array(
            'course_register_id' => $this->course_register_id,
            'number_of_registrants' => $this->number_of_registrants,
            'payment_status' => $this->payment_status,
            'user_id' => $this->user_id,
            'course_id' => $this->course_id
        );
        return $this->db->get_all('course_register',$data);
    }

    public function find($course_register_id)
    {
        $query_data = $this->db->get_where('courseregis', ["course_register_id" => $course_register_id]);

        if($query_data->num_rows() ==1)
        {
            $query_data = $query_data->row();

            $new_obj = clone $this;
            $new_obj->course_register_id = $query_data->course_register_id;
            $new_obj->number_of_registrants = $query_data->number_of_registrants;
            $new_obj->payment_status = $query_data->payment_status;
            $new_obj->user_id = $query_data->user_id;
            $new_obj->course_id = $query_data->course_id;
        }else{
            $new_obj = FALSE;
        }
        return $new_obj;

    }

}