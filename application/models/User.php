<?php
/**
 * Created by PhpStorm.
 * User: Sejan
 * Date: 8/4/2017
 * Time: 2:26 PM
 */

class User extends CI_Model
{
    public $user_id;
    public $title_name = "";
    public $name = "";
    public $surname = "";
    public $birthday = "";
    public $citizen_id = "";
    public $department = "";
    public $telephone_number = "";
    public $telephone_number_department = "";
    public $username="";
    public $password = "";
    public $user_type_id ="";
    public $activated ="";


    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data)
    {

        $citizen_id =$data['citizen_id'];

        $this->db->where('citizen_id',$citizen_id);
        $query = $this->db->get('user');

        if($query ->row()>0)
        {

                echo "<script>alert('ข้อมูลมีการลงทะเบียนแล้ว');setTimeout(function () {
                window.location.href= '".site_url("home")."';},1500);</script>";
        }else{

            $this->db->insert('user', $data);
                echo "<script>alert('ลงทะเบียนสำเร็จแล้ว');setTimeout(function () {
                window.location.href= '".site_url("home")."';},1500);</script>";
        }

        return $this->db->insert_id();

    }

    public function sendEmail($to_Email)
    {
        $from_email = 'team@mydomain.com'; //change this to yours
        $subject = 'Verify Your Email Address';
        $message = 'Dear User,<br /><br />Please click on the below activation link to verify your email address.<br /><br /> http://www.mydomain.com/user/verify/' . md5($to_Email) . '<br /><br /><br />Thanks<br />Mydomain Team';

        //configure email settings
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.mydomain.com'; //smtp host name
        $config['smtp_port'] = '465'; //smtp port number
        $config['smtp_user'] = $from_email;
        $config['smtp_pass'] = '********'; //$from_email password
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n"; //use double quotes
        $this->email->initialize($config);

        //send mail
        $this->email->from($from_email, 'Mydomain');
        $this->email->to($to_Email);
        $this->email->subject($subject);
        $this->email->message($message);
        return $this->email->send();

    }
    public function verifyEmailID($key)
    {
        $data = array('status' => 1);
        $this->db->where('md5(email)', $key);
        return $this->db->update('user', $data);
    }
    public function checkUser_Pass($citizen_id)
    {

        $result = $this->db->where('citizen_id',$citizen_id)->get('user');

        return $result->num_rows() >0 ? $result->row()->user_id : FALSE;
    }


    public function update()
    {
        $data = array(
            'title_name' => $this->title_name,
            'name' => $this->name,
            'surename' => $this->surname,
            'birthday' => $this->birthday,
            'citizen_id' => $this->citizen_id,
            'department' => $this->department,
            'telephone_number' => $this->telephone_number,
            'telephone_number_department' => $this->telephone_number_department,
            'password' => $this->password,
            'user_type_id' => $this->user_type_id

        );
        return $this->db->replace('user', $data);
    }

    public function delete()
    {
        $data = array(
            'title_name' => $this->title_name,
            'name' => $this->name,
            'surename' => $this->surname,
            'birthday' => $this->birthday,
            'citizen_id' => $this->citizen_id,
            'department' => $this->department,
            'telephone_number' => $this->telephone_number,
            'telephone_number_department' => $this->telephone_number_department,
            'password' => $this->password,
            'user_type_id' => $this->user_type_id

        );
        return $this->db->delete('user', $data);
    }

    public function select()
    {
        $data = array(
            'title_name' => $this->title_name,
            'name' => $this->name,
            'surename' => $this->surname,
            'birthday' => $this->birthday,
            'citizen_id' => $this->citizen_id,
            'department' => $this->department,
            'telephone_number' => $this->telephone_number,
            'telephone_number_department' => $this->telephone_number_department,
            'password' => $this->password,
            'user_type_id' => $this->user_type_id

        );
        return $this->db->get('user', $data);
    }

    public function select_all()
    {
        $data = array(
            'title_name' => $this->title_name,
            'name' => $this->name,
            'surename' => $this->surname,
            'birthday' => $this->birthday,
            'citizen_id' => $this->citizen_id,
            'department' => $this->department,
            'telephone_number' => $this->telephone_number,
            'telephone_number_department' => $this->telephone_number_department,
            'username'=> $this->username,
            'password' => $this->password,
            'user_type_id' => $this->user_type_id

        );
        return $this->db->get_all('user', $data);
    }

    public function find($user_id)
    {
        $query_data = $this->db->get_where('user', ["user_id" => $user_id]);

        if($query_data->num_rows() ==1)
        {
            $query_data = $query_data->row();

            $new_obj = clone $this;
            $new_obj->user_id = $query_data->user_id;
            $new_obj->title_name = $query_data->title_name;
            $new_obj->name = $query_data->name;
            $new_obj->surname = $query_data->surname;
            $new_obj->birthday = $query_data->birthday;
            $new_obj->citizen_id = $query_data->citizen_id;
            $new_obj->department = $query_data->department;
            $new_obj->telephone_number = $query_data->telephone_number;
            $new_obj->telephone_number_department = $query_data->telephone_number_department;
            $new_obj->email = $query_data->email;
            $new_obj->username = $query_data->username;
            $new_obj->password = $query_data->password;
            $new_obj->activated = $query_data->activated;
            $new_obj->user_type_id = $query_data->user_type_id;
        }else{
            $new_obj = FALSE;
        }
        return $new_obj;

    }


    Public function get_all()
    {
        $query = $this->db->get('user');
        return $query->row();

        return $query->result();
    }

    public function _checkUser($username,$password)
    {
        $result = $this->db->where('username',$username)->where('password',$password)->get('user');
        return $result->num_rows() >0 ? $result->row()->user_id : FALSE;
    }


    public function edit($data,$user_id)
    {
        $this->db->update('user', $data, array('user_id' =>$user_id));
        echo "<script>alert('แก้ไขข้อมูลสำเร็จแล้ว');setTimeout(function () {
                window.location.href= '".site_url("home")."';},1500);</script>";

    }
    public function change_password($data,$user_id)
    {
        $this->db->update('user', $data, array('user_id' =>$user_id));
        $this->session->sess_destroy();
        echo "<script>alert('เปลี่ยนรหัสผ่านสำเร็จแล้ว กรุณาล็อกอินใหม่ด้วยคะ');setTimeout(function () {
                window.location.href= '".site_url("home")."';},1500);</script>";
    }
    public function get($user_id)
    {
        $query = $this->db->get_where("user",array("user_id"=>$user_id));

        $data = $query->result();

        return $data;
    }
}

