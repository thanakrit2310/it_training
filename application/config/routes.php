<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'page/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['home'] = "Page/home"; // หน้าแรก
$route['homeadmin'] = "Page/HomeAdmin"; // หน้าแรกadmin
$route['contactus'] = "Page/contactus"; // หน้าติดต่อเจ้าหน้าที่
$route['history'] = "HistoryIT/history"; // หน้าประวัติความเป็นมาองค์กร
$route['login'] = "Login/dologin"; // หน้าล็อกอิน
$route['logout'] = "Login/logout"; // หน้าล็อกเอ้า
$route['register'] = "Register/index"; // หน้าลงทะเบียนสมาชิก
$route['user/register'] = "Register/register";//
$route['ittraining'] = "Ittraining/ittraining";// หน้าข้อมูลการอบรม
$route['ajax'] = "Register/ajax";
//ส่วนของผู้ใช้
$route['profile'] = "Profile/profile"; //หน้าข้อมูลส่วนตัว
$route['user/Training'] = "Training/training" ; //ประวัติการอบรม
$route['user/(:num)/(:num)/confirm'] = "User_confirm/confirm/$1/$1"; //หน้ายืนยันการลงทะเบียนเข้าอบรม
$route['user/(:num)/(:num)/payment'] = "User_confirm/payment/$1/$1"; //หน้ายืนยันชำระเงิน
$route['profile_edit'] = "Profile_edit/profile_edit";//หน้าแก้ไขโปรไฟล์
$route['change_password'] = "Change_password/change_password";//หน้าเปลี่ยนรหัสผ่าน
$route['assessment'] ="Assessment/assessment";// หน้าประเมินการอบรม
//ส่วนของโครงการอบรม
$route['course/(:num)/detail'] = "Course_controllers/detail/$1"; //หน้าแสดงรายละเอียดของหลักสูตร